package redisDataBase;

import redis.clients.jedis.JedisPool;

class RedisPool {

    private static JedisPool pool;

    public static JedisPool getPool() {
        if (pool == null) {
            pool = new JedisPool("localhost");
            System.out.println("Connected to Redis.");
        }

        return pool;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        pool.destroy();
    }
    
    

}
