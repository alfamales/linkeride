package redisDataBase;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author michael
 */
public class RedisDataServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession currentClientSession = request.getSession();

        if (isClientBelongToRedisServlet(currentClientSession)) {
            handleClient(currentClientSession, request, response);
        } else {
            response.sendRedirect(""); //TODO - user try to hack in our server!
        }
    }

    private static boolean isClientBelongToRedisServlet(HttpSession currentClientSession) {
        ClientStatus clientRequest = (ClientStatus) currentClientSession.getAttribute("ClientRequest");
        return !currentClientSession.isNew() && clientRequest == ClientStatus.REDIS_REQUEST;
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void handleClient(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ClientRedisRequest clientRedisRequest = (ClientRedisRequest) currentClientSession.getAttribute("ClientRedisRequest");

        switch (clientRedisRequest) {
            case CREATE_NEW_DRIVER_RIDE:
                getServletContext().getRequestDispatcher("/createNewRideForDriverServlet").forward(request, response);
                break;
            case CREATE_NEW_PASSENGER_RIDE:
                getServletContext().getRequestDispatcher("/createNewRideForPassengerServlet").forward(request, response);
                break;
            case DELETE_DRIVER_RIDE:
                getServletContext().getRequestDispatcher("/deleteNewRideForDriverServlet").forward(request, response);
                break;
            case DELETE_PASSENGER_RIDE:
                getServletContext().getRequestDispatcher("/deleteNewRideForPassengerServlet").forward(request, response);
                break;
            case APROVE_PASSENGER_BY_DRIVER:
                getServletContext().getRequestDispatcher("/approvePassengerByDriverServlet").forward(request, response);
                break;
            case GET_DATA_ON_DRIVERS_RIDE:
                getServletContext().getRequestDispatcher("/getDataOnDriversRideServlet").forward(request, response);
                break;
            case GET_DATA_ON_PASSENGERS_RIDE:
                getServletContext().getRequestDispatcher("/getDataOnPassengersRideServlet").forward(request, response);
                break;
            case SEARCH_FOR_DRIVER_WITH_ACTIVE_RIDE:
                getServletContext().getRequestDispatcher("/searchForDriverWithActiveRidesServlet").forward(request, response);
                break;
            case SEND_DRIVER_JOIN_REQUEST:
                getServletContext().getRequestDispatcher("/passengerSendsDriverJoinRequestServlet").forward(request, response);
                break;
            case UPDATE_DRIVER_RIDE:
                getServletContext().getRequestDispatcher("/updateActiveRideForDriverServlet").forward(request, response);
                break;
            case UPDATE_PASSENGER_RIDE:
                getServletContext().getRequestDispatcher("/updateActiveRideForPassengerServlet").forward(request, response);
                break;
            case RETURN_TO_MAIN_SERVLET:
                reportSuccessToMainServlet(currentClientSession, request, response);
                break;
            case REPORT_FAILED:
            default:
                reportFailureToMainServlet(currentClientSession, request, response);
        }
    }

    private void reportToMainServlet(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentClientSession.setAttribute("ClientRequest", ClientStatus.MAIN_SERVLET);
        currentClientSession.setAttribute("ClientRedisRequest", ClientRedisRequest.RETURN_TO_MAIN_SERVLET);
        getServletContext().getRequestDispatcher("/mainServlet").forward(request, response);
    }

    private void reportFailureToMainServlet(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("RequestStatus",false);
        reportToMainServlet(currentClientSession, request, response);
    }

    private void reportSuccessToMainServlet(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("RequestStatus",true);
        reportToMainServlet(currentClientSession, request, response);
    }

}
