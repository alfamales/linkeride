
package redisDataBase;

import rideData.DriverActiveRideData;
import rideData.RideTimeRange;
import rideData.TimeFormatException;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import rideData.BadRideDataExeption;

/**
 *
 * @author michael
 */
public class CreateNewRideForDriverServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession currentClientSession = request.getSession();

        if (isClientBelongToThisServlet(currentClientSession)) {
            handleClientRequest(currentClientSession, request, response);
        } else {
            response.sendRedirect(""); //TODO - user try to hack in our server!
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean isClientBelongToThisServlet(HttpSession currentClientSession) {
        ClientRedisRequest clientRequest = (ClientRedisRequest) currentClientSession.getAttribute("ClientRedisRequest");

        return !currentClientSession.isNew() && clientRequest == ClientRedisRequest.CREATE_NEW_DRIVER_RIDE;
    }

    private void handleClientRequest(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DriverActiveRideData clientRequest = (DriverActiveRideData) currentClientSession.getAttribute("DriverActiveRideData");

        if (isValidData(clientRequest)) {
            startNewDriversActiveRide(clientRequest,currentClientSession,request,response);
        } else {
            reportFailure(currentClientSession, request, response);
        }
    }

    private boolean isValidData(DriverActiveRideData clientRequest) {
        return clientRequest.isValidDataForCreatingNewActiveRide()
                && isNewRequest(clientRequest);
    }

    private void startNewDriversActiveRide(DriverActiveRideData clientRequest, HttpSession currentClientSession,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Jedis redis = null;

        try {
            redis = addNewDriveresActiveRideToRedis(redis, clientRequest);
            reportSucces(clientRequest,currentClientSession,request,response);
        } catch (JedisConnectionException | BadRideDataExeption e) {
            if (redis != null) {
                RedisPool.getPool().returnBrokenResource(redis);
                redis = null;
            }
            System.out.println(e);
        } finally {
            if (redis != null) {
                RedisPool.getPool().returnResource(redis);
            }
        }    
    }

    private Jedis addNewDriveresActiveRideToRedis(Jedis redis, DriverActiveRideData clientRequest) throws BadRideDataExeption {
        redis = RedisPool.getPool().getResource();
        long rideID = redis.incr("global-ride-id");
        redis.set("ride:"+rideID+":time", clientRequest.getFinishRoundTime().toString());
        redis.set("ride:" + rideID +":date", clientRequest.getDriveDate().toString());
        redis.set("ride:" + rideID + ":driver", Integer.toString(clientRequest.getDriverID()));
        redis.set("ride:"+ rideID + ":loc",clientRequest.getRidePath().toString());
        redis.set("ride:" + rideID + ":maxpasn",Integer.toString(clientRequest.getMaxNumberOfPassengers()));
        
        //TODO update all global data with this ride
        for(int i=0; i<clientRequest.getRideTimeRange().getRideTimeLength();i++){
            redis.sadd("ride:"
                    +clientRequest.getDriveDate().toString()
                    +":"+clientRequest.getRideTimeRange().getRideTimeStampByIndex(i)
                    +":"+clientRequest.getRidePath().toString()
                    , Long.toString(rideID));
        }
        
        return redis;
    }

    private void reportFailure(HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentClientSession.setAttribute("ClientRedisRequest", ClientRedisRequest.REPORT_FAILED);
        getServletContext().getRequestDispatcher("/redisDataServlet").forward(request, response);
    }

    private boolean isNewRequest(DriverActiveRideData clientRequest) {
        Jedis redis = null;
        boolean isNewRequest = true;
        
        try {
            redis = RedisPool.getPool().getResource();
            Set<String> activeRides = redis.smembers("uid:" + clientRequest.getDriverID() + ":ride");
            
            if (activeRides != null) {
                for (String activeRideID : activeRides) {
                    if (isSimilarRide(activeRideID, clientRequest, redis)) {
                        isNewRequest = false;
                    }
                }
            }

            return isNewRequest;
        } catch (JedisConnectionException e) {
            if (redis != null) {
                RedisPool.getPool().returnBrokenResource(redis);
                redis = null;
            }
            throw e;
        } finally {
            if (redis != null) {
                RedisPool.getPool().returnResource(redis);
            }
        }
    }

    private boolean isSimilarRide(String activeRideID, DriverActiveRideData clientRequest, Jedis redis) {
        String timeRange = redis.get("sride:" + activeRideID + ":time");
        String date = redis.get("sride:" + activeRideID + ":date");
        
        return isSameDate(clientRequest, date)
                && isInTimeRange(clientRequest,timeRange);
    }

    private static boolean isSameDate(DriverActiveRideData clientRequest, String date) {
        return clientRequest.getDriveDate().toString().equals(date);
    }

    private boolean isInTimeRange(DriverActiveRideData clientRequest, String timeRange) {
        try {
            RideTimeRange rideRange = new RideTimeRange(timeRange);
            return rideRange.isInSameTimeRange(clientRequest.getRideTimeRange());
        } catch (TimeFormatException ex) {
            return false;
        }
    }

    private void reportSucces(DriverActiveRideData clientRequest, HttpSession currentClientSession, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentClientSession.setAttribute("ClientRedisRequest", ClientRedisRequest.RETURN_TO_MAIN_SERVLET);
        getServletContext().getRequestDispatcher("/redisDataServlet").forward(request, response);
    }

}
