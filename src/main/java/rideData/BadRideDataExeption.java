package rideData;

/**
 *
 * @author michael
 */
public class BadRideDataExeption extends Exception {
    
    private static final String ERROR_MSG = "bad ride data";
    
    public BadRideDataExeption() {
    }

    @Override
    public String toString() {
        return "TimeFormatException{" + "errorMSG=" + ERROR_MSG + '}';
    }
    
    
}
