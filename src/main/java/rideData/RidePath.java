package rideData;

/**
 *
 * @author michael
 */
public class RidePath {
    private City destination;
    private City origin;

    public RidePath(City destination, City origin) throws BadRideDataExeption {
        if(destination!=origin){
            this.destination = destination;
            this.origin = origin;
        }else{
            throw new BadRideDataExeption();
        }
        
    }

    public City getDestination() {
        return destination;
    }

    public void setDestination(City destination) {
        this.destination = destination;
    }

    public City getOrigin() {
        return origin;
    }

    public void setOrigin(City origin) {
        this.origin = origin;
    }

    /**
     * @return origin:destination
     */
    @Override
    public String toString() {
        return String.format("%s:%s",origin,destination);
    }
    
    
    
    
}
