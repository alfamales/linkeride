package rideData;

/**
 *
 * @author michael
 */
public class RideDate {
    private int month;
    private int day;

    public RideDate(int month, int day) throws BadRideDataExeption {
        if(month>=1 && month<=12 && day>=1 && day <=31){
            this.month = month;
            this.day = day;
        }else{
            throw new BadRideDataExeption();
        }
    }

    /**
     * @return mm:dd
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d",month,day);
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
