package rideData;

/**
 *
 * @author michael
 */
public class RideTime {

    private int hour;
    private int minutes;

    public RideTime(int hour, int minutes) throws TimeFormatException {
        trySetTime(hour, minutes);
    }

    /**
     * @param timeString hh:mm
     * @throws rideData.TimeFormatException
     */
    public RideTime(String timeString) throws TimeFormatException {
        try {
            tryCreateTime(timeString);
        } catch (NumberFormatException e) {
            throw new TimeFormatException();
        }
    }

    public static RideTime addMinutes(RideTime startTime, int minutesToAdd) throws TimeFormatException {
        if (isLegalMinutes(minutesToAdd)) {
            int totalMinutes = startTime.hour * 60 + startTime.minutes + minutesToAdd;
            totalMinutes = totalMinutes % 1440; //remove days
            int hour = totalMinutes % 60;
            int minutes = totalMinutes - hour * 60;

            return new RideTime(hour, minutes);
        } else {
            throw new TimeFormatException();
        }
    }

    public int getRideTimeSetsAmount() {
        return hour * 4 + minutes / 15;
    }

    private void tryCreateTime(String timeString) throws NumberFormatException, TimeFormatException {
        String[] timeParts = timeString.split(":");

        if (timeParts.length == 2) {
            int newHour = Integer.parseInt(timeParts[0]);
            int newMinute = Integer.parseInt(timeParts[1]);
            trySetTime(newHour, newMinute);
        } else {
            throw new TimeFormatException();
        }
    }

    private void trySetTime(int hour1, int minutes1) throws TimeFormatException {
        if (isLegalHours(hour1) && isLegalMinutes(minutes1)) {
            this.hour = hour1;
            this.minutes = minutes1;
        } else {
            throw new TimeFormatException();
        }
    }

    private static boolean isLegalMinutes(int minutes) {
        return minutes == 0 || minutes == 15 || minutes == 30 || minutes == 45;
    }

    private static boolean isLegalHours(int hour) {
        return hour >= 0 && hour <= 23;
    }

    /**
     * @return hh:mm
     */
    @Override
    public String toString() {
        return String.format("%d:%d", hour, minutes);
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) throws TimeFormatException {
        if (isLegalHours(hour)) {
            this.hour = hour;
        } else {
            throw new TimeFormatException();
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) throws TimeFormatException {
        if (isLegalMinutes(minutes)) {
            this.minutes = minutes;
        } else {
            throw new TimeFormatException();
        }
    }

    @Override
    public int hashCode() {
        int hash = hour * 51 + minutes * 23;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RideTime other = (RideTime) obj;
        if (this.hour != other.hour) {
            return false;
        }
        if (this.minutes != other.minutes) {
            return false;
        }
        return true;
    }

    public boolean isBigger(RideTime time) {
        if (hour > time.hour) {
            return true;
        } else if (hour == time.hour && minutes > time.minutes) {
            return true;
        } else {
            return false;
        }
    }

    boolean isSmaller(RideTime time) {
        if (equals(time)) {
            return false;
        } else {
            return !isBigger(time);
        }
    }

}
