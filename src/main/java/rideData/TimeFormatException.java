package rideData;

/**
 *
 * @author michael
 */
public class TimeFormatException extends Exception {

    private static final String ERROR_MSG = "Wrong time formal exception";
    
    public TimeFormatException() {
    }

    @Override
    public String toString() {
        return "TimeFormatException{" + "errorMSG=" + ERROR_MSG + '}';
    }
}
