package rideData;

/**
 *
 * @author michael
 */
public enum City {
    TEL_AVIV("Tel_Aviv"),
    HAIFA("Haifa"),
    JERUSALEM("Jerusalem"),
    HOLON("Holon"),
    ASHDOD("Ashdod"),
    ASHKELON("Ashkelon"),
    BEER_SHEVA("Beer_Sheva"),
    EILAT("Eilat"),
    TVERIYA("Tveriya"),
    NETANIA("Netania"),
    BAT_YAM("Bat_Yam"); //TODO: to make more :)
    
    private final String cityStr;
    
    private City(String cityString){
        cityStr = cityString;
    }
    

    @Override
    public String toString() {
        return cityStr;
    }
    
}
