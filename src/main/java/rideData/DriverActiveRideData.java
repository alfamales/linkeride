package rideData;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author michael
 */
public class DriverActiveRideData {

    private int driverID = NOT_SET_VALUE;
    private int rideID = NOT_SET_VALUE;
    private RideDate driveDate;
    private List<PassengerActiveRideData> passengersOnWaitingList = new ArrayList<>();
    private List<PassengerActiveRideData> passengersWasOnRide = new ArrayList<>();
    private int maxNumberOfPassengers = NOT_SET_VALUE;
    private RidePath ridePath;
    private RideTimeRange rideTimeRange;

    private static final int NOT_SET_VALUE = -1;

    public DriverActiveRideData() {
    }

    public void setRidePath(RidePath ridePath) {
        this.ridePath = ridePath;
    }

    public boolean isRideIDSet() {
        return rideID != NOT_SET_VALUE;
    }

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public RideDate getDriveDate() {
        return driveDate;
    }

    public void setDriveDate(RideDate driveDate) {
        this.driveDate = driveDate;
    }

    public List<PassengerActiveRideData> getPassengersOnWaitingList() {
        return passengersOnWaitingList;
    }

    public void setPassengersOnWaitingList(List<PassengerActiveRideData> passengers) {
        this.passengersOnWaitingList = passengers;
    }

    public int getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }

    public void setMaxNumberOfPassengers(int maxNumberOfPassengers) throws BadRideDataExeption {
        if(maxNumberOfPassengers>=1 || maxNumberOfPassengers <=10){
            this.maxNumberOfPassengers = maxNumberOfPassengers;
        }else{
            throw new BadRideDataExeption();
        }
    }

    public City getDestination() {
        return ridePath.getDestination();
    }

    public City getOrigin() {
        return ridePath.getOrigin();
    }

    public RidePath getRidePath() {
        return ridePath;
    }

    public RideTime getStartRoundTime() {
        return rideTimeRange.getStartTime();
    }

    public RideTime getFinishRoundTime() {
        return rideTimeRange.getEndTime();
    }

    public RideTimeRange getRideTimeRange() {
        return rideTimeRange;
    }

    public void setRideTimeRange(RideTimeRange rideTimeRange) {
        this.rideTimeRange = rideTimeRange;
    }

    public int getRideID() {
        return rideID;
    }

    public void setRideID(int rideID) {
        this.rideID = rideID;
    }

    public List<PassengerActiveRideData> getPassengersWasOnRide() {
        return passengersWasOnRide;
    }

    public void setPassengersWasOnRide(List<PassengerActiveRideData> passengersWasOnRide) {
        this.passengersWasOnRide = passengersWasOnRide;
    }

    public boolean isValidDataForCreatingNewActiveRide() {
        return isValidTime()
                && isLocationValid()
                && isDateValid()
                && !isRideIDSet()
                && isNoPassengers()
                && isLegalMaxNumberOfPassengers()
                && isDriverIDExist();
    }

    private boolean isValidTime() {
        return rideTimeRange != null;
    }

    private boolean isLocationValid() {
        return ridePath != null;
    }

    private boolean isDateValid() {
        return driveDate != null;
    }

    private boolean isNoPassengers() {
        return passengersOnWaitingList.isEmpty();
    }

    private boolean isLegalMaxNumberOfPassengers() {
        return maxNumberOfPassengers != NOT_SET_VALUE;
    }

    private boolean isDriverIDExist() {
        return driverID != NOT_SET_VALUE;
    }

}
