package rideData;

/**
 *
 * @author michael
 */
public class RideTimeRange {

    private RideTime startTime;
    private RideTime endTime;

    public RideTimeRange(RideTime startTime, RideTime endTime) throws TimeFormatException {
        if (isGoodTimesRelations(startTime, endTime)) {
            this.startTime = startTime;
            this.endTime = endTime;
        } else {
            throw new TimeFormatException();
        }
    }

    private boolean isGoodTimesRelations(RideTime startTime1, RideTime endTime1) {
        return startTime1.getHour() < endTime1.getHour()
                && startTime1.getMinutes() < endTime1.getHour();
    }

    /**
     * @param timeRange hh:mm-hh:mm
     * @throws rideData.TimeFormatException
     */
    public RideTimeRange(String timeRange) throws TimeFormatException {
        String[] timeParts = timeRange.split("-");
        if (timeParts.length == 2) {
            startTime = new RideTime(timeParts[0]);
            endTime = new RideTime(timeParts[1]);
        }
    }

    public boolean isInSameTimeRange(RideTimeRange time) {
        return isInTimeRangeHelper(time.startTime)
                || isInTimeRangeHelper(time.endTime);
    }

    private boolean isInTimeRangeHelper(RideTime time) {
        return startTime.isBigger(time) && endTime.isSmaller(time);
    }

    public RideTime getStartTime() {
        return startTime;
    }

    public void setStartTime(RideTime startTime) throws TimeFormatException {
        if (endTime.isBigger(startTime)) {
            this.startTime = startTime;
        } else {
            throw new TimeFormatException();
        }
    }

    public RideTime getEndTime() {
        return endTime;
    }

    public void setEndTime(RideTime endTime) throws TimeFormatException {
        if (startTime.isSmaller(endTime)) {
            this.endTime = endTime;
        } else {
            throw new TimeFormatException();
        }
    }

    /**
     * @return hh:mm-hh:mm
     */
    @Override
    public String toString() {
        return String.format("%d:%d-%d:%d",
                startTime.getHour(), startTime.getMinutes(),
                endTime.getHour(), endTime.getMinutes());
    }

    public int getRideTimeLength() {
        return startTime.getRideTimeSetsAmount() - endTime.getRideTimeSetsAmount();
    }

    public String getRideTimeStampByIndex(int index) throws BadRideDataExeption {
        if (index >= 0 && index < getRideTimeLength()) {
            try {
                RideTime timeStamp = RideTime.addMinutes(startTime, index * 15);
                return timeStamp.toString();
            } catch (TimeFormatException e) {
                System.out.println(e);
            }
        } else {
            throw new BadRideDataExeption();
        }
        return null;
    }
}
