package rideData;

/**
 *
 * @author michael
 */
public class PassengerActiveRideData {
    private int passengerID = NOT_SET_VALUE;
    private int driverID = NOT_SET_VALUE;
    private int sRideID = NOT_SET_VALUE;
    private RideDate driveDate;
    private RidePath ridePath;
    private RideTimeRange rideTimeRange;
    
    private static final int NOT_SET_VALUE = -1;

    public PassengerActiveRideData() {
    }

    public int getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(int passengerID) {
        this.passengerID = passengerID;
    }

    public int getDriverID() {
        return driverID;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public int getSRideID() {
        return sRideID;
    }

    public void setSRideID(int searchRideID) {
        this.sRideID = searchRideID;
    }

    public RideDate getDriveDate() {
        return driveDate;
    }

    public void setDriveDate(RideDate driveDate) {
        this.driveDate = driveDate;
    }

    public RidePath getRidePath() {
        return ridePath;
    }

    public void setRidePath(RidePath ridePath) {
        this.ridePath = ridePath;
    }

    public RideTimeRange getRideTimeRange() {
        return rideTimeRange;
    }

    public void setRideTimeRange(RideTimeRange rideTimeRange) {
        this.rideTimeRange = rideTimeRange;
    }
    
    
}
